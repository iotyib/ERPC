cmake_minimum_required(VERSION 3.8)

add_executable(app2 app.c)
add_executable(service2 service.c)
target_link_libraries(app2 ${Libraries})
target_link_libraries(service2 ${Libraries})
