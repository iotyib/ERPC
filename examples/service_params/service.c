#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "erpc.h"

cJSON *hello_world_service(cJSON *params)
{
    char *message = NULL;
    cJSON *result = NULL;

    /* deal with params : print params */
    message = cJSON_Print(params);
    printf("params: %s\n", message);
    free(message);
    cJSON_Delete(params);

    /* response remote */
    result = cJSON_CreateObject();
    cJSON_AddStringToObject(result, "hello", "I'm helloworld service.");

    return result;
}

int main(void)
{
    erpc_framework_init("service");

    erpc_service_register("hello", "helloworld", hello_world_service);

    return erpc_framework_loop(ERPC_LOOP_DEFAULT);
}

